/* ---------------------------	
	Init nav bar show toggle		
------------------------------------------------------------------------------------------------------- */

$(function() {

	var checkScroll = function (){
	    if( $(window).width() > 992 ){
		    var $header = $("#primary-nav");
	    	if (window.location.pathname === '/' || 
	    		window.location.pathname.indexOf('description') > -1 ||
	    		window.location.pathname.indexOf('for-owners') > -1 ){	    		
				
				var scroll = $(window).scrollTop();
		        if (scroll >= 150) {
		            $header.addClass("show");
		            $header.find('.logo a.desktop img').attr('src', $header.find('.logo a.desktop img').data('scroll-image') )
		        } else {
		            $header.removeClass('show');
		            $header.find('.logo a.desktop img').attr('src', $header.find('.logo a.desktop img').data('reset-image') )
		        }

	    	} else {
	    		$header.addClass("show no-shadow");
	    		$header.find('.logo a.desktop img').attr('src', $header.find('.logo a.desktop img').data('scroll-image') )
	    		if(! $('[data-fixed-toggle="true"]') && $(window).width() <= 992 ){	    			
	    			$('body').css('padding-top', $header.outerHeight(true) );
	    		}
	    	}
	    } else {
	    	if(! $('[data-fixed-toggle="true"]') ){
	    		$('body').attr('style', '' );
	    	}
	    }
	}

	checkScroll();		
    $(window).on('scroll resize', function() {
		checkScroll();	        
    });

});


/* ---------------------------	
	IOS STOP ZOOM		
------------------------------------------------------------------------------------------------------- */

// document.documentElement.addEventListener('touchmove', function (event) {
//     event.preventDefault();      
// }, false);


$(document).ready( function (argument) {
	

	/* =====================================
		
		COMMON	
		
	================================================================================================================ */
	
	/* ---------------------------	
		INIT BOOTSTRAP TOOLTIP				
	------------------------------------------------------------------------------------------------------- */

	$(function () {		
		$('[data-toggle="tooltip"]').tooltip()		
	});


	/* ---------------------------	
		FIXED TOGGLE		
	------------------------------------------------------------------------------------------------------- */
	
	$(function(){

		var $toggle = $('[data-fixed-toggle="true"]');

		if( $toggle.length) {

			var $window = $(window);
			
			toggleFixed($window);

			$window.on('resize scroll', function(){
				toggleFixed( $(this) );				
			});
		}

		function toggleFixed ( $w ){
			if( $w.width() <= 992 ) {
				$('body').css({
					paddingBottom: $toggle.outerHeight(true),
				});
				if( $w.scrollTop() > 100 ) {
					$toggle.addClass('show');
				} else {
					$toggle.removeClass('show');
				}
			} else {
				$('body').css({
					paddingBottom: 0,
				});
			}

		}

	});


	/* ---------------------------	
		SCROLL TO LOCATION		
	------------------------------------------------------------------------------------------------------- */
	
	$(function () {

		var $scroll = $('[data-scroll-to]');
		if( $scroll.length > 0 ){
			$scroll.on('click', function() {
				var $target = $(this).data('scroll-to');
				console.log( $target );
				$('html, body').animate({
			        scrollTop: $( $target ).offset().top - 100
			    }, 800);

			});
		}

	});


	/* ---------------------------	
		WOW - Animate on scroll			
	------------------------------------------------------------------------------------------------------- */
	
	$(function () {
		
		new WOW({
	    	mobile: false,
	    }).init();

	});


	/* ---------------------------	
		MOBILE NAV		
	------------------------------------------------------------------------------------------------------- */
	
	$(function () {
		
		$('#mobile-link-ham').on('click', function(){
			$('html, body, #mobile-menu, #mobile-link-ham').toggleClass('nav-open');
		})

		// $(document).click(function() {
		//     if( $('body, #mobile-menu').hasClass('nav-open') ){
		//     	$('body, #mobile-menu').removeClass('nav-open');
		//     }
		// });
		// $('#mobile-menu').click(function(e) {
		//     e.stopPropagation();
		//     return false;
		// });

	})




	/* =====================================
		
		HOME PAGE	
		
	================================================================================================================ */
		
	/* ---------------------------	
		Init featured houses		
	------------------------------------------------------------------------------------------------------- */
	
	var $featuredHousesSlider = $('#featured-houses');

	if( $featuredHousesSlider.length ) {
		
		$featuredHousesSlider.slick( {
			slidesToShow: 6,
			slidesToScroll: 6,
			dots: false,
			centerMode: false,
			focusOnSelect: true,
			accessibility: true,

			responsive: [
			{
		      breakpoint: 1600,
		      settings: {
		        slidesToShow: 4,
		        slidesToScroll: 4,
		      }
		    },
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		        centerMode: false,
		      }
		    },
		    {
		      breakpoint: 980,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2,
		        centerMode: false,
		      }
		    },
		     {
		      breakpoint: 580,
		      settings: {
		      	centerPadding: 0,
		        slidesToShow: 1,
		        slidesToScroll: 1,
		      }
		    }
		  ]

		});

		$("#featured-houses .property-node .image-holder")
			.cycle({
	            fx:     'fade',
	            speed:   800,
	            timeout: 800,
	            pause:   800,
	        })
	    	.cycle('pause')
	    	.hover(
	        	function() {
	            	$(this).cycle('resume');
	        	},
		        function(){
		            $(this).cycle('pause');
		        }
	     	);

	}

	var $fullSlider = $('#custom-slider');

	if( $fullSlider.length ) {
		
		var data 	= [];
		var $target = $('#hero-home');

		$fullSlider.find('li').each( function() {			
			data.push({
				img: 	$(this).data('img'),
				title:  $(this).data('title'),
				para:   $(this).data('para'),
			});
		});
	
		var renderSlide = function(data) {
			
			$target.css({
				backgroundImage: 'url("' + data.img + '")'
			});

			$target.find('#title').html(data.title);
			$target.find('#para').html(data.para);
		}


		/* first slide is already show */
		var i = 1;
		setInterval( function() {;
			if( i >= data.length){
				i = 0;
				renderSlide( data[0] );
			} else {
				renderSlide( data[i] );
			}
			i++;

		}, 7000);

	}


	/* ---------------------------	
		Init Testimonial Slider		
	------------------------------------------------------------------------------------------------------- */
	
	var $testimonialSlider = $('#testimonial-slider');

	if( $testimonialSlider.length ) {
		
		$testimonialSlider.slick( {
			slidesToShow: 2,
			slidesToScroll: 1,
			dots: false,
			accessibility: false,

			responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		      }
		    }
		  ]

		});

	}

	var $testimonialSliderOwner = $('#testimonial-slider-owner');

	if( $testimonialSliderOwner.length > 0) {
		$testimonialSliderOwner.slick( {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			accessibility: false,
		})
	}

	/* ---------------------------	
		Init Why Us Slider		
	------------------------------------------------------------------------------------------------------- */
	
	var $whyUsSlider = $('#why-us-slider');

	if( $whyUsSlider.length ) {

		$whyUsSlider.slick( {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			autoplay: true,
			accessibility: false,
		});	

	}

	
	/* ---------------------------	
		Toggle components		
	------------------------------------------------------------------------------------------------------- */

	if( $(window).width() > 992 ) {
		$('.jumbo-search-wrapper.mobile').html('')
	} else {
		$('.jumbo-search-wrapper.desktop').html('')
	}
	



 	/* =====================================
 		
 		LISTING PAGE	
 		
 	================================================================================================================ */
 	
 	
 	var $listingPage = $('#listing-container');
 	if( $listingPage.length ) {

 		/* ---------------------------	
 			SET HEIGHT OF MAP CONTAINER	
 			This method will help in setting the height and margin dynamically.	
 		------------------------------------------------------------------------------------------------------- */
 		
 		var setListingContainerHeight = function() {
 			var finalHeight = $('#primary-nav').outerHeight(true) + $('#primary-filters').outerHeight(true)  ;
 			finalHeight 	+= 'px';
 			
 			if( $(window).width() > 992 ) {
 				$('#property-node-container, #map-wrapper').css({
	 				height: 'calc(100vh - ' + finalHeight + ')',
	 				marginTop: finalHeight,
	 			});
 			} else {
 				$('#property-node-container').css({
	 				marginTop: $('#primary-filters').outerHeight(true) + 'px',
	 			});

 			}
 			
 		}

 		var triggerGridView = function() {
 			if( $(window).width() <= 992 ) {
 				$('#property-node-container').removeClass('map-view');
	 			$('#map-wrapper').addClass('hidden');
	 			$('#listing-container').addClass('container');
 			}  			
 		}

 		setListingContainerHeight();
 		triggerGridView();

 		$(window).on('resize', function() {
 			setListingContainerHeight();
 			triggerGridView();
 		});


 		/* ---------------------------	
 			WHEN USER CLICK ON REMOVE FILTER CLOSE BTN		
 		------------------------------------------------------------------------------------------------------- */
 		
 		$('#applied-filters .close').on('click', function(){
 			
 			$(this).parent().remove();

 			if( $('#applied-filters li').length > 1 ) {
 				$('#reset-filters').show();
 			} else {
 				$('#reset-filters').hide();
 			}

 			setListingContainerHeight();

 		});


 		/* ---------------------------	
 			WHEN USER CLICKS ON RESER FILTERS BTN		
 		------------------------------------------------------------------------------------------------------- */
 		
 		$('#reset-filters').on('click', function(){
 			$('li .close').parent().remove();
 			$(this).hide();
 			setListingContainerHeight();
 		})


		/* ---------------------------	
			CHANGE NODE STYLE				
		------------------------------------------------------------------------------------------------------- */

 		$('#change-node-style').on('click', function(){
 			
 			$('#property-node-container').toggleClass('map-view');
 			$('#map-wrapper').toggleClass('hidden');
 			$('#listing-container').toggleClass('container');

 			$(this).html(
 				$(this).text() === 'Map View' ? 'Grid View' : 'Map View' 
 			);

 		});



 		/* ---------------------------	
 					
 		------------------------------------------------------------------------------------------------------- */
 		if( $(window).width() > 992 ){
			var handlesSlider = document.getElementById('slider-range--1');
 		} else {
			var handlesSlider = document.getElementById('slider-range--2');
 		}

		noUiSlider.create(handlesSlider, {
			start: [ 5000, 100000 ],
			step: 1000,
			animate: true,
			animationDuration: 300,

			range: {
				'min': [  5000 ],
				'max': [ 100000 ]
			}
		});

		handlesSlider.noUiSlider.on('update', function( values, handle ) {
			$('#min-range').val( Number(values[0]) );
			$('#max-range').val( Number(values[1]) );
		});

		$('#apply-range').on('click', function(){
			handlesSlider.noUiSlider.set([ Number($('#min-range').val()), Number($('#max-range').val()) ]);
		});
	}




	/* =====================================
		
		DESCRIPTION PAGE	
		
	================================================================================================================ */
	
	$descriptionPage = $('#desctiption-container');

	if( $descriptionPage.length > 0) {
		

		/* ---------------------------	
			INIT LIGHTBOX		
		------------------------------------------------------------------------------------------------------- */
				
		var gallery = $('.room-gallery a').simpleLightbox();
		
		/* ---------------------------	
			SHOW LIGHTBOX WHEN HERO BANNER IS CLICKS		
		------------------------------------------------------------------------------------------------------- */
		
		$('#open-gallery-lightbox').on('click', function(){
			gallery.open();
		});



	}





	/* =====================================
		
		list-my-property-container	
		
	================================================================================================================ */
	
	var $listProperty = $('#list-my-property-container');

	if( $listProperty.length > 0 ) {
		
		function render() {

			var width 			= '',
			index 			= 1;

			var $step 			= $('.step:visible'),
				$stepReel 		= $('.step-wizad-reel:visible'),
				$stepContainer 	= $('.step-wizad-container:visible');
			
			var stepCount		= $step.length;

			adjustWidth();

			var $btnFinish 		= $('.btn-finish'),
				$btnPrevious	= $('.btn-previous'),
				$btnContinue 	= $('.btn-continue');

			$btnContinue.on('click', function(){
				
				if( validate() === false ) {
					return;
				}

				if( index > $step.length-1 ) {
					return;
				}
				var move = '-' + ( (width + index + 20) * index ) + 'px';
				$stepReel.css({
					transform: 'translateX(' + move + ')'
				});
				index++;
				switchButtons( index );		

			});

			$btnPrevious.on('click', function(){
				
				if( validate() === false ) {
					return;
				}

				if( index == 2 ){
					index = 1;
					var move = '0px';				
				} else {
					console.log( width );
					var move = '-' + ( width + 30 ) + 'px';
					index--;
				}

				$stepReel.css({
					transform: 'translateX(' + move + ')'
				});
				switchButtons( index );

			});

			$btnFinish.on('click', function(){
				
				if( validate() == false ) {

				}

				$stepReel.hide();
				$('.button-wrapper').hide();
				$('.completed')
					.addClass('animated zoomIn')
					.removeClass('hide')

			});

			function adjustWidth() {			
				
				width = $stepContainer.width();	
				$step.width( width );
				$stepReel.width( (width + 20) * 3 );

			}

			function switchButtons( index ) {
				
				var data = $step.eq( index-1 );
				
				var curr = data.data('current-step'),
					prev = data.data('previous-step'),
					next = data.data('next-step');
				
				
				/* Show Prev */
				if( prev == 'NULL' && curr && next ) {
					$btnFinish.hide();
					$btnPrevious.hide();
					$btnContinue.show();
				} 

				/* Show next */
				if( prev != 'NULL' && curr && next != 'NULL') {
					$btnFinish.hide();
					$btnPrevious.show();
					$btnContinue.show();
				}

				/* Show Finish*/
				if( prev && curr && next == 'NULL') {
					$btnFinish.show();
					$btnPrevious.show();
					$btnContinue.hide();
				}

			}

			$(window).on('resize', function(){			
				adjustWidth();		
			});

			function validate(){
				return true;
			}
		}

		render();
		
		$('#modal-list-my-property').on('shown.bs.modal', function () {
			setTimeout( function() {
				render();
			}, 400)

			console.log( $step );

		});

	}





	/* =====================================
		
		ACCOUNT MODALS	
		
	================================================================================================================ */
	
	var $accModal = $("[data-show-form]");

	if( $accModal.length > 0 ) {

		$accModal.on('click', function(){
			var formID 		= $(this).data('show-form');
			var formHeader 	= $(this).data('form-header');

			$('#modal-account form').removeClass('d-block').addClass('d-none')
			$(formID).removeClass('d-none').addClass('d-block');
			$('#modal-account #section-headline-text').html( formHeader );
			
			$(formID).find('input:first').focus();

		});

		$('#account-login-form').on('submit', function(e){
			e.preventDefault();

			$('#alert-success')
				.addClass('slide-down')
				.find('.message').html('<strong>Voila!</strong>, Welcome back');				
						
			$(this).trigger("reset");

		});
		
		$('#account-recover-form').on('submit', function(e){
			e.preventDefault();

			$('#alert-error')
				.addClass('slide-down')
				.find('.message').html('<strong>Oops!</strong>, Seems like we could not find that email account');				
						

		});

	}

});