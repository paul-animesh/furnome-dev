function dataSubmit(url, data, btn_elem, callback)
{
	console.log('here');
	if(btn_elem != false) {
		var html = btn_elem.html();
		btn_elem.html('Processing...'); 
		btn_elem.attr('disabled','disabled');
	}
	$.ajax({
		url : url,
		type : 'POST',
		data: data,
        dataType: 'json',
		beforeSend: function() {
		
		},	
		complete: function() {
			if(btn_elem != false) {
				btn_elem.removeAttr('disabled');
				btn_elem.html(html);
			}
		},
		success: function(json) {
			callback(json);			
		},
		error: function() {
			
		}
	});
}

function validateformSubmit(url, div_id, btn_elem, callback)
{
	if($('#'+div_id).parsley().validate()) {
		var html = btn_elem.html();
		btn_elem.html('Processing...'); 
		btn_elem.attr('disabled','disabled');
		data = $('#'+div_id).serialize();
		$.ajax({
			url : url,
			type : 'POST',
			data: data,
	        dataType: 'json',
			beforeSend: function() {
			
			},	
			complete: function() {
				btn_elem.removeAttr('disabled');
				btn_elem.html(html);
			},
			success: function(json) {
				callback(json);			
			},
			error: function() {
				
			}
		});
	} else {
		return false;
	}
}

function formSubmit(url, div_id, btn_elem, callback) {
	
	var html = btn_elem.html();
	btn_elem.html('Processing...'); 
	btn_elem.attr('disabled','disabled');
	data = $('#'+div_id).serialize();
	$.ajax({
		url : url,
		type : 'POST',
		data: data,
        dataType: 'json',
		beforeSend: function() {
		
		},	
		complete: function() {
			btn_elem.removeAttr('disabled');
			btn_elem.html(html);
		},
		success: function(json) {
			callback(json);			
		},
		error: function() {
			
		}
	});
}

function fileupload(formData,url) {
	// console.log('hello',formData)
	// alert('asdas')
	// var form = $('#'+div_id)[0];
	// var formData = new FormData(form);
	$.ajax({
		url: url,
		type: 'POST',
	    data: formData,
	    // THIS MUST BE DONE FOR FILE UPLOADING
	    contentType: false,
	    processData: false,
	    beforeSend: function() {
			$('.dropify-clear').click();
			UIkit.modal("#spinner_dialogue",{bgclose:false}).show();
		},	
		complete: function() {
			
		},
		success: function(json) {
			if (json.success === 1) {
				UIkit.modal("#spinner_dialogue").hide();
				location.reload()
			} else {
				UIkit.modal("#spinner_dialogue").hide();
       			UIkit.notify({
				    message : 'Some error occured while saving data',
				    status  : 'danger',
				    timeout : 5000,
				    pos     : 'top-center'
				});
			}
		},
		error: function() {
			
		}
	});
}

function resetcallback(data) {
	if (data.success === 1) {
		$('#reset_success').text(data.msg);
		$('#reset_success_div').fadeIn('280');
		setTimeout(function(){
			$('#reset_success_div').fadeOut('280');
			$('#reset_success').text('');
		},3000);
	} else {
		$('#reset_error').text(data.msg);
		$('#reset_error_div').fadeIn('280');
		setTimeout(function(){
			$('#reset_error_div').fadeOut('280');
			$('#reset_error').text('');
		},3000);
	}
}

function checkmail(data) {
	if(data.success === 1) {
		$('#email_check').fadeOut('200');
		$('#register_email').val(data.email);
		$('#register_email').attr('readonly',true);
		$('#signup_form').fadeIn('200');
	} else if(data.success === 0) {
		$('#checkemail_error').html(data.msg);
		$('#checkemail_error_div').fadeIn('280');
		setTimeout(function(){
			$('#checkemail_error_div').fadeOut('280');
			$('#checkemail_error').text('');
		},3000);
	}else if (data.success === 2) {
		$('#checkemail_error').html(data.msg+' <a href="/subscribe">Subscribe</a> to get an invite');
		$('#checkemail_error_div').fadeIn('280');
	}
}

function downloadfile(id, btn_elem) {
	UIkit.modal("#spinner_dialogue",{bgclose:false}).show();
	var data = new Array();
	data.push({name:'token', value: id });
	dataSubmit('/api/filedownlaod', data, btn_elem, checkfiledownload);
}

function checkfiledownload(data) {
	if (data.success === 1) {
		var polldata = new Array();
		polldata.push({name: 'token', value: data.token});
		polldata.push({name: 'file', value: data.file});
		dataSubmit('/api/filedownloadcheck', polldata, false, checkfiledownload);
	} else if(data.success === 0) {
		UIkit.modal("#spinner_dialogue").hide();
		var filedata=data.file;
	  	$('<form action="/api/download" method="POST">' + 
	    	'<input type="hidden" name="file" value="' + filedata+ '">' +
	    	'</form>').appendTo('body').submit();
	} else {
		UIkit.modal("#spinner_dialogue").hide();
		console.log('Some error has occured');
	}
}

function resetcomplete(data) {
	if (data.success === 1) {
		$('#reset_success').text(data.msg);
		$('#reset_success_div').fadeIn('280');
		setTimeout(function(){
			$('#reset_success_div').fadeOut('280');
			$('#reset_success').text('');
			window.location.href = '/login';
		},3000);
	} else {
		$('#reset_error').text(data.msg);
		$('#reset_error_div').fadeIn('280');
		setTimeout(function(){
			$('#reset_error_div').fadeOut('280');
			$('#reset_error').text('');
		},3000);
	}
}

function deletefile(id, btn_elem) {
	UIkit.modal("#spinner_dialogue",{bgclose:false}).show();
	var data = new Array();
	data.push({name:'token', value: id });
	dataSubmit('/api/filedelete', data, btn_elem, completefiledelete);
}

function completefiledelete(data) {
	if (data.success === 1 ) {
		if ($('#file_'+data.token).length > 0) {
			altair_md.init();
			$('#file_'+data.token).remove();
			altair_md.init();
			UIkit.modal("#spinner_dialogue").hide();
		}
	} else {
		UIkit.modal("#spinner_dialogue").hide();
	}
}