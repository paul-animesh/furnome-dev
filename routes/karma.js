var express = require('express');
var router = express.Router();


function IsAuthenticated(req,res,next){
    if(typeof(req.session) === 'undefined' || !req.session.user){
        next();
    }else{
        res.redirect('/console/');
    }
}
/* GET home page. */
router.get('/', IsAuthenticated, function(req, res, next) {
	res.render('login');
});



module.exports = router;
