const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
const User = mongoose.model('User')
const Validate = mongoose.model('Validate')
const randomstring = require("randomstring")
const validator = require('validator')
const nodemailer = require('nodemailer')
const sgTransport = require('nodemailer-sendgrid-transport')
const crypto = require('crypto')

router.post('/signin', (req, res) => {
	var username = req.body.username
	var password = req.body.password
	User.findOne({ 'username' :  username }, (err, user) => {
		// In case of any error, return using the done method
		if (err) {
			console.log('Error in SignIn: '+err)
			return res.send({success: 0, msg: "Some error occured. Please Try again later"})
		}

		// Username does not exist, log the error and redirect back
		if (!user){
			return res.send({success: 0, msg: "User not found"})
		}
		// User exists but wrong password, log the error 
		if (!isValidPassword(user, password)){
			return res.send({success: 0, msg: "Invalid password"})
		}
		// which will be treated like success
		let sess = {
			id: user._id,
			username: user.username,
			email: user.email,
			type: user.type
		}
		// req.login(sess, function(error) {
		// 	if (error) {
		// 		console.log('Error in creating session user signin: ' + error)
		// 		return res.send({success: 0, msg: "Some error occured. Please Try again later"})
		// 	}
		// 	return res.send({success:1})
		// })

		if(!req.session.user)
			req.session.user = sess

		return res.send({success:1})
	})
})

router.post('/signup',(req, res) => {
	let username = req.body.username
	let password = req.body.password
	let email = req.body.email
	let password_repeat = req.body.password_repeat
	User.findOne({$or:[{ 'username' :  username },{'email' :email}]}, function(err, user) {
		// In case of any error, return using the done method
		if (err){
			console.log('Error in SignUp: '+err)
			return res.send({success: 0, msg: "Some error occured. Please Try again later"})
		}
		// already exists
		if (user) {
			if(user.username == username && user.email != email) {
				return res.send({success: 0, msg:'Username already exists. Please choose a different username.'})
			} else if(user.username != username && user.email == email) {
				return res.send({success: 0, msg:'Email already exists.'})
			} else {
				return res.send({success: 0, msg:'User already exists.'})
			}
		} else {
			// check if password is matching the repeat password
			if(password === password_repeat && password != '') {
				// if there is no user, create the user
				var newUser = new User()

				// set the user's local credentials
				let salt = randomstring.generate({charset: "alphanumeric", length: 5})
				newUser.username = username
				newUser.salt = salt
				newUser.password = createHash(password, salt)
				newUser.email = email
				// save the user
				newUser.save(function(saveerr) {
					if (saveerr){
						console.log('Error in saving user: ' + saveerr)
						return res.send({success: 0, msg: "Some error occured. Please Try again later"})
					}
					var sess = {
						id: newUser._id,
						username: newUser.username,
						email: newUser.email,
						type: newUser.type
					}

					// req.login(sess, function(error) {
					// 	if (error) {
					// 		console.log('Error in creating session user signup: ' + error)
					// 		return res.send({success: 0, msg: "Some error occured. Please Try again later"})
					// 	}
					// 	return res.send({success:1})
					// })
					if(!req.session.user)
						req.session.user = sess

					return res.send({success:1})
				})
			} else {
				return res.send({success:0, msg:'Password do not match'})
			}
		}
	})
})

router.post('/resetrequest',function(req, res, next){
	var instance_array = ['http://127.0.0.1:3000','https://www.merak.ai']
	if(instance_array.indexOf(req.headers.origin) !== -1){
		User.findOne({'username':req.param('login_username_reset')}).select({email: 1, _id: 1}).exec(function(err,user){
			if (user) {
				var email_id = user.email
				if (validator.isEmail(email_id)) {
					var options = {
						auth: {
							api_user: 'adinesh',
							api_key: ''
						}
					}
					var validate = new Validate()
					validate.user_id = user._id
					rand_string = randomstring.generate()
					validate.validation = rand_string
					validate.valid = '1'
					var client = nodemailer.createTransport(sgTransport(options))
					var email = {
						from: 'Merak abhinava@merak.ai',
						to: email_id,
						subject: 'Reset Password',
						html: '<!DOCTYPE html><html><head><title></title></head><body>Hi,<br/><br/>Your request for reseting your password has been successful.<br/><br/>Please follow the following link to reset your password '+req.headers.origin+'/reset?validate='+rand_string+'.<br/><br/>Link will expire in 24 hours. If you haven\'t requested to change your password please ignore.<br/><br/>Thanks,<br/><strong>Team Merak</strong></body></html>'
					}

					validate.save(function(err) {
						if (err){
							console.log('Error: '+err)  
							// throw err
							return
						}
						client.sendMail(email, function(err, info){
							if (err ){
								console.log(error)
								res.json({success: 0, msg: 'Error in sending mail. Please try again.'})
							}
							else {
								console.log('Message sent: ' + info.response)
								res.json({success: 1, msg: 'Mail has been sent to your email id. Please follow the instructions.'})
							}
						})
					})
				} else {
					res.json({success: 0, msg: 'Invalid email. Please contact admin at abhinava@merak.ai'})
				}
			} else {
				res.json({success: 0, msg: 'User is not registered'})
			}
		})
	} else {
		res.json({success: 0, msg: 'Invalid request'})
	}
})

router.post('/reset',function(req,res,next){
	if (req.body.password === req.body.password_repeat) {
		Validate.findOne({validation: req.body.token, valid: '1'}).select({user_id: 1}).exec(function(err, user) {
			if (err) {
				console.log(err)
			}
			if(user) {
				user.valid = '0'
				user.save(function(err){
					if (err) {
						console.log('Error in updating validation key: '+err)  
						throw err
					}
					User.update({_id: user.user_id}, { password: createHash(req.body.password) }, {}, function(err){
						if (err) {
							console.log('Error updating password: '+err)  
							throw err
						}
						res.json({success: 1, msg: 'Password updated successfully. Please login using new password'})
					})
				})
			} else {
				res.json({success: 0, msg: 'Error in updating password. If problem persist, please mail to abhinava@cloudbrewlabs.com'})
			}
		})
	} else {
		res.json({success: 0, msg: 'Passwords do not match'})
	}
})

//log out
router.get('/signout', function(req, res) {
	req.session.destroy()
	res.redirect('/')
})

module.exports = router

var isValidPassword = function(user, password){
	return crypto.createHash('sha256', user.salt).update(password).digest('hex') === user.password
}
// Generates hash using bCrypt
var createHash = function(password, salt){
	return crypto.createHash('sha256', salt).update(password).digest('hex');
}