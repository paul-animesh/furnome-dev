var express = require('express');
var router = express.Router();
const mongoose = require('mongoose')
const Owner = mongoose.model('Owner')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey("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");

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('for-owners',{
            meta_title: 'List Your Property | Rent Your House at Furnome',
            meta_desc: 'Find the right tenant for your house now! Only Furnome promises rent on sign-up and end-to-end apartment management.'
        });
});


router.post('/enquiry', function(req, res, next) {
  
    var data = req.body
    var owner = new Owner()
    owner.owner_name = data.owner_name
  	owner.owner_number = data.owner_number
  	owner.owner_email = data.owner_email
  	owner.owner_address = data.owner_address
  	owner.owner_apartment_name = data.owner_apartment_name
  	owner.owner_apartment_floor = data.owner_apartment_floor
  	owner.owner_apartment_rooms = data.owner_apartment_rooms
  	owner.owner_apartment_availability = data.availability
  	owner.owner_apartment_rent = data.rent
  	owner.owner_apartment_deposit = data.deposit
  	owner.owner_apartment_date_availability = data.date_available
    // owner.owner_apartment_amenities = data.amenities
    
    owner.save((err) => {
        if(err) {
            res.send({success: 0})
        } else {
            sendMail(data)
            res.send({success: 1})
        }
    })
});

function sendMail(data){
    var msg = {
      to: 'info@furnome.com',
      from: 'info@furnome.com',
      // to: 'ankitbindal0@gmail.com',
      // from: 'ankitbindal0@gmail.com',
      subject: 'Owner listing Alert',
      text: "Name: "+data.owner_name+"<br>Email: "+data.owner_email+"<br>Phone: "+data.owner_number+"<br>Address: "+data.owner_address,
      html: "<html><body><table><tbody><tr><td>Name</td><td>"+data.owner_name+"</td></tr><tr><td>Email</td><td>"+data.owner_email+"</td></tr><tr><td>Phone</td><td>"+data.owner_number+"</td></tr><tr><td>Address</td><td>"+data.owner_address+"</td></tr><tr><td>Apartment Name</td><td>"+data.owner_apartment_name+"</td></tr></tbody></table></body></html>",
    };
    sgMail.send(msg);

    msg = {
      to: data.owner_email,
      from: 'info@furnome.com',
      subject: 'Contact Us | Furnome',
      text: "Dear "+data.owner_name+", We have received your message. Our Executive will shortly get in touch with you",
      html: "Dear "+data.owner_name+",<br><br> We have received your message. Our Executive will shortly get in touch with you.<br><br>Regards,<br>Furnome Team",
    };
    sgMail.send(msg);
        
}

module.exports = router;
