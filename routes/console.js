const mongoose = require('mongoose')
const User = mongoose.model('User')
const Owner_enquiry = mongoose.model('Owner')
const House = mongoose.model('House')
const Locality = mongoose.model('Locality')
const Enquiry = mongoose.model('Enquiry')
const Tenant = mongoose.model('Tenant')
const express = require('express')
const router = express.Router()
const randomstring = require("randomstring")
const crypto = require('crypto')
const multiparty = require('multiparty')
const async = require('async')
const fs = require('fs')
const csv = require("fast-csv")
const springedge = require('springedge')
const sgMail = require('@sendgrid/mail')
sgMail.setApiKey("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")
// var elasticsearch = require('elasticsearch');
// var client = new elasticsearch.Client({
//   host: 'localhost:9200',
//   log: 'trace'
// })

// client.ping({
// 	// ping usually has a 3000ms timeout
// 	requestTimeout: 2000
// }, function (error) {
// if (error) {
// 	console.trace('elasticsearch cluster is down!');
// } else {
// 	console.log('All is well');
// }
// });


function IsAuthenticated(req,res,next){
    if(req.session.user){
        next()
    }else{
        res.redirect('/')
    }
}

router.get('/', IsAuthenticated,  (req, res) => {
    if(req.session.user.type == 'superadmin') {
		// res.render('console')
		User.find({"type": "admin"}).exec((err, users) => {
            if(err) {
                console.log("Some error occured while fetching the organisations")
                return res.send('console',{orgs: []})
			}
			res.render('console',{users: users})
        })
    } else if(req.session.user.type == 'admin') {
		House.find({}).exec((err, houses) => {
			if(err) {
				res.render('listing', {houses: []})
			}
			res.render('listing', {houses: houses})
		})
    }
})

router.get('/add', IsAuthenticated, (req, res) => {
    if(req.session.user.type == 'superadmin') {
        res.render('add')
    } else if(req.session.user.type == 'admin') {
        res.redirect('/console')
    }
})

router.get('/newhouse', IsAuthenticated, (req, res) => {
	if(req.session.user.type == 'admin') {
        res.render('house')
    } else if(req.session.user.type == 'superadmin') {
        res.redirect('/console')
    }
})

router.get('/edit/:id', IsAuthenticated, (req, res) => {
	let id = req.params.id
	House.findOne({_id: id}).exec((err, house) => {
		if(err) {
			res.redirect('/console')
		}
		res.render('edithouse', {house: house})
	})
})

router.get('/owner_enquiry', IsAuthenticated, (req, res) => {
	if(req.session.user.type == 'admin') {
		Owner_enquiry.find({}).exec((err, owner_enquiry_details) => {
            if(err) {
                console.log("Some error occured while fetching the owner enquiry")
                res.redirect('/console')
			}
			// console.log(owner_enquiry_details)
            res.render('owner_enquiry',{details: owner_enquiry_details})
        })
        // res.render('owner_enquiry')
    } else if(req.session.user.type == 'superadmin') {
        res.redirect('/console')
    }
})

router.get('/house_enquiry', IsAuthenticated, (req, res) => {
	if(req.session.user.type == 'admin') {
		Enquiry.find({}).sort({date_created: -1}).exec((err, house_enquiry_details) => {
            if(err) {
                console.log("Some error occured while fetching the house enquiry")
                res.redirect('/console')
			}
			// console.log(house_enquiry_details)
            res.render('house_enquiry',{details: house_enquiry_details})
        })
        // res.render('owner_enquiry')
    } else if(req.session.user.type == 'superadmin') {
        res.redirect('/console')
    }
})

router.get('/rent', IsAuthenticated, (req, res) => {
	if(req.session.user.type == 'admin') {
        res.render('rent')
    } else if(req.session.user.type == 'superadmin') {
        res.redirect('/console')
    }
})

router.get('/tenant', IsAuthenticated, (req, res) => {
	if(req.session.user.type == 'admin') {
		Tenant.find({}).sort({date_created: -1}).exec((err, tenants) => {
            if(err) {
                console.log("Some error occured while fetching the house enquiry")
                res.redirect('/console')
			}
			res.render('tenant', {tenants: tenants})
		})
    } else if(req.session.user.type == 'superadmin') {
        res.redirect('/console')
    }
})

router.post('/rentupload', IsAuthenticated, function(req, res, next){
	var form = new multiparty.Form()
	form.parse(req, function(err, fields, files) {
		//Request file information
      	//{ fieldName: 'uploaded_file',  originalFilename: 'kapil.jpg',  path: '/tmp/x8y45YxeWhtihLJ97wd46_1P.jpg',  headers:    { 'content-disposition': 'form-data; name="uploaded_file"; filename="kapil.jpg"',     'content-type': 'image/jpeg' },  size: 13636 }
      	var timestamp = Date.now();
      	// if(files.uploaded_file[0].size > 0 && files.uploaded_file[0].size < 2097152) {
      	if(files.uploaded_file[0].size > 0 && files.uploaded_file[0].size < 115343360) {
			let filepath = __dirname + "/../public/upload/" +timestamp+'_'+files.uploaded_file[0].originalFilename
			fs.rename(files.uploaded_file[0].path, filepath, function(err) {
				if (err) {
					console.log(err);
					return;
				}
				csv
				.fromPath(filepath, {headers : true})
				.on("data", function(data){
					// console.log(data)
					let number = data["Tenant Number"].split(",")
					let email = data["Tenant Email Id"].split(",")
					let pnumber = []
					let pemail = []
					let j = 0
					for(var i in number) {
						if(number[i] != "" && number[i].trim().length == 10) {
							pnumber[j] = number[i].trim()
							j++
						}
					}

					j = 0
					for(var i in email) {
						if(email[i] != "" && validateEmail(email[i].trim())) {
							pemail[j] = email[i].trim()
							++j
						}
					}

					let name = data["Tenant Name"].split(" ")
					if(pnumber.length > 0) {
						let tenant = {
							'sender': 'FRNOME',
							'apikey': '36432xa4p75464z7nbnmrsc9a8d70d81qj4',
							// 'to': [
							// 	//data["Tenant Number"]  //Moblie Numbers 
							// 	"9036305152"
							// ],
							'to': pnumber, 
							'message': "Hi " + name[0] + ", Gentle Reminder for rent. Make payment by 5th to avoid late payment of Rs.100/day. Share snap/ref no. on 9901752255 if paid - TEAM FURNOME",
							'format': 'json'
						}

						springedge.messages.send(tenant, 5000, function (errc, responsec) {
							if (errc) {
								console.log(errc)
								res.redirect("/console")
							}
						})
					}

					if(pemail.length > 0) {
						var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];;
						var date = new Date();
						var msg = {
							// to: data["Tenant Email Id"],
							// to: "adinesh42@gmail.com",
							to: pemail,
							from: 'noreply@furnome.com',
							subject: 'URGENT: RENT REMINDER',
							html: "<html><body>Dear " + name.join(" ") + ", <br><br> Greetings from Team Furnome. <br> This is a gentle reminder for the rent payment of " + data["Flat No."] + ", " + data["Building Name"] + " for " + months[date.getMonth()] + " " + date.getFullYear() + ". Kindly make the payment on or before 5th to avoid late payment charges of INR 100 per day. <br><br> Request you to share a snapshot or reference number (mentioning your name and flat number) on whatsapp 9901752255 if paid or after you are making the payment. <br><br>Cheers!<br>Team Furnome</body></html>",
						};
						sgMail.send(msg);
					}
				})
				.on("end", function(){
					res.send({success: 1})
				});
			});
	    } else {
	    	fs.unlink(files.uploaded_file[0].path,function() {
				// file deleted
			});
			res.json({success: 0, msg: 'Max upload file size is 8MB'});
	    }
    });
});

function validateEmail(email) {
    var re = /^(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
    return re.test(String(email).toLowerCase());
}

router.post('/create', IsAuthenticated, (req, res) => {
    let username = req.body.username
	let password = 'Furnome@123'
	let email = req.body.email
    let password_repeat = 'Furnome@123'
    let orgname = req.body.orgname
	User.findOne({$or:[{ 'username' :  username },{'email': email}]}, function(err, user) {
		// In case of any error, return using the done method
		if (err){
			console.log('Error in SignUp: '+err)
			return res.send({success: 0, msg: "Some error occured. Please Try again later"})
		}
		// already exists
		if (user) {
			if(user.username == username && user.email != email) {
				return res.send({success: 0, msg:'Username already exists. Please choose a different username.'})
			} else if(user.username != username && user.email == email) {
				return res.send({success: 0, msg:'Email already exists.'})
			} else {
				return res.send({success: 0, msg:'User already exists.'})
			}
		} else {
			// check if password is matching the repeat password
			if(password === password_repeat && password != '') {
				// if there is no user, create the user
				var newUser = new User()

				// set the user's local credentials
				let salt = randomstring.generate({charset: "alphanumeric", length: 5})
				newUser.username = username
				newUser.salt = salt
				newUser.password = createHash(password, salt)
				newUser.email = email
				// save the user
				newUser.save(function(saveerr) {
					if (saveerr){
						console.log('Error in saving user: ' + saveerr)
						return res.send({success: 0, msg: "Some error occured. Please Try again later"})
                    }
					return res.send({success:1})
				})
			} else {
				return res.send({success:0, msg:'Password do not match'})
			}
		}
	})
})

// router.post('/resumable', function(req, res, next){
// 	var form = new multiparty.Form() 
//     form.parse(req, function(err, fields, files) {
//     	fs.rename(files.file[0].path, 'uploads/'+fields.resumableFilename, function(err) {

//     		if (err) {
//     			console.log(err)
//     		}
//     		res.json({success:1})
//     	})
//     })
// })

router.get('/deletehouse/:id', IsAuthenticated, (req, res) => {
	let id = req.params["id"]
	House.findOneAndUpdate({_id: id}, {$set: {deleted: true}}).exec((err, result) => {
		res.redirect("/console")
	})
})

router.get('/putback/:id', IsAuthenticated, (req, res) => {
	let id = req.params["id"]
	House.findOneAndUpdate({_id: id}, {$set: {deleted: false}}).exec((err, result) => {
		res.redirect("/console")
	})
})

router.get('/disablehouse/:id', IsAuthenticated, (req, res) => {
	let id = req.params["id"]
	House.findOneAndUpdate({_id: id}, {$set: {status: false}}).exec((err, result) => {
		res.redirect("/console")
	})
})

router.get('/enablehouse/:id', IsAuthenticated, (req, res) => {
	let id = req.params["id"]
	House.findOneAndUpdate({_id: id}, {$set: {status: true}}).exec((err, result) => {
		res.redirect("/console")
	})
})

router.post('/addhouse', IsAuthenticated, (req, res) => {	
	var form = new multiparty.Form() 
    form.parse(req, function(err, fields, files) {
    	let images = files.images
		let imagelist = []
		async.forEachOfSeries(images, (value, key, callback) => {
			let timestamp = Date.now()
			fs.rename(value.path, __dirname + "/../public/upload/" + timestamp + "_" + value.originalFilename, function(err) {
				if (err) {
					callback(err)
				}
				imagelist.push(timestamp + "_" + value.originalFilename)
				callback(null)
			})
		}, (err) => {
			if(err) {
				res.send({success: 0})
			} else {
				async.waterfall([
					function(callback) {
						Locality.findOne({locality: fields.locality[0]}).exec((err, locality) => {
							if(err) {
								callback(err, null)
							} else {
								if(!locality) {
									let locality = new Locality()
									locality.locality = fields.locality[0]
									locality.save((error) => {
										if(error) {
											callback(error)
										} else {
											callback(null)
										}
									})
								} else {
									callback(null)
								}
							}
						})
					},
					function(callback) {
						let house = new House()
						house.name = fields.name[0]
						house.area = parseInt(fields.area[0])
						house.description = fields.description[0]
						house.latitude = fields.latitude[0]
						house.longitude = fields.longitude[0]
						house.address = fields.address[0]
						house.locality = fields.locality[0]
						house.inclusion = fields.inclusion[0]
						house.exclusion = fields.exclusion[0]
						house.wow = fields.wow[0]
						house.property_type = fields.ptype[0]
						house.property_rooms = fields.proom[0]
						house.room_count = parseInt(fields.roomcount[0])
						house.property_for = fields.pfor[0]
						house.poc_name = fields.poc_name[0]
						house.poc_contact = fields.poc_contact[0]
						house.location_url = fields.short_url[0]
						let amenities = fields.amenities
						for(index in amenities) {
							house.amenities.push(amenities[index])	
						}
						
						let details = fields.details
						for(index in details) {
							let detail = JSON.parse(details[index])
							house.details.push(detail)
							house.prices.push({"rent": parseInt(detail.rent)})
						}

						for(index in imagelist) {
							house.images.push(imagelist[index])
						}

						if(fields.booked[0] === '0') {
							house.booked = false
						} else if(fields.booked[0] === '1') {
							house.booked = true
						}

						house.save((err) => {
							if(err) {
								callback(err)
								// res.send({success: 0})
							} else {
								// client.create({
								// 	index: 'furnome',
								// 	type: 'houses',
								// 	id: house._id,
								// 	body: { 
								// 		name:  house.name, 
								// 		latitude: house.latitude, 
								// 		longitude: house.longitude, 
								// 		locality: house.locality, 
								// 		property_type: house.property_type, 
								// 		property_rooms: house.property_rooms, 
								// 		property_for: house.property_for, 
								// 		amenities: house.amenities,
								// 		rent: house.prices,
								// 		area: house.area, 
								// 		booked: house.booked
								// 	}
								// }, function (err, resp) {
								// res.send({success: 1})
								// });
								callback(null)
							}
						})		
					}
				], function (err) {
					if(err) {
						console.log(err)
						res.send({success: 0})
					} else {
						res.send({success: 1})
					}
				})
			}
		})
    })
})

router.post('/edithouse', IsAuthenticated, (req, res) => {	
	var form = new multiparty.Form() 
    form.parse(req, function(err, fields, files) {
		let images = files.images
		if(typeof(images) != "undefined" && images.length > 0) {
			let imagelist = []
			async.forEachOfSeries(images, (value, key, callback) => {
				let timestamp = Date.now()
				fs.rename(value.path, __dirname + "/../public/upload/" + timestamp + "_" + value.originalFilename, function(err) {
					if (err) {
						callback(err)
					}
					imagelist.push(timestamp + "_" + value.originalFilename)
					callback(null)
				})
			}, (err) => {
				if(err) {
					res.send({success: 0})
				} else {
					async.waterfall([
						function(callback) {
							Locality.findOne({locality: fields.locality[0]}).exec((err, locality) => {
								if(err) {
									callback(err, null)
								} else {
									if(!locality) {
										let locality = new Locality()
										locality.locality = fields.locality[0]
										locality.save((error) => {
											if(error) {
												callback(error)
											} else {
												callback(null)
											}
										})
									} else {
										callback(null)
									}
								}
							})
						},
						function(callback) {
							let amenities = fields.amenities
							let aminityarr = new Array()
							for(index in amenities) {
								aminityarr.push(amenities[index])	
							}

							let details = fields.details
							let detailsarr = new Array()
							let prices = new Array()
							for(index in details) {
								let detail = JSON.parse(details[index])
								detailsarr.push(detail)
								prices.push({"rent": parseInt(detail.rent)})
							}

							let imagearr = new Array()
							for(index in imagelist) {
								imagearr.push(imagelist[index])
							}

							let booked = false
							if(fields.booked[0] === '0') {
								booked = false
							} else if(fields.booked[0] === '1') {
								booked = true
							}

							House.findOneAndUpdate({_id: fields.id[0]}, {$set: {name: fields.name[0], area: parseInt(fields.area[0]), description: fields.description[0], latitude: fields.latitude[0], longitude: fields.longitude[0], address: fields.address[0], locality: fields.locality[0], inclusion: fields.inclusion[0], exclusion: fields.exclusion[0], wow: fields.wow[0], property_type: fields.ptype[0], property_rooms: fields.proom[0], room_count: parseInt(fields.roomcount[0]), property_for: fields.pfor[0], poc_name: fields.poc_name[0], poc_contact: fields.poc_contact[0], location_url: fields.short_url[0], amenities: aminityarr, details: detailsarr, prices: prices, images: imagearr, booked: booked}}).exec((updateerr, updateresp) => {
								if(updateerr) {
									callback(updateerr)
								} else {
									callback(null)
								}
							})		
						}
					], function (err) {
						if(err) {
							console.log(err)
							res.send({success: 0})
						} else {
							res.send({success: 1})
						}
					})
				}
			})
		} else {
			async.waterfall([
				function(callback) {
					Locality.findOne({locality: fields.locality[0]}).exec((err, locality) => {
						if(err) {
							callback(err, null)
						} else {
							if(!locality) {
								let locality = new Locality()
								locality.locality = fields.locality[0]
								locality.save((error) => {
									if(error) {
										callback(error)
									} else {
										callback(null)
									}
								})
							} else {
								callback(null)
							}
						}
					})
				},
				function(callback) {
					let amenities = fields.amenities
					let aminityarr = new Array()
					for(index in amenities) {
						aminityarr.push(amenities[index])	
					}

					let details = fields.details
					let detailsarr = new Array()
					let prices = new Array()
					for(index in details) {
						let detail = JSON.parse(details[index])
						detailsarr.push(detail)
						prices.push({"rent": parseInt(detail.rent)})
					}

					let booked = false
					if(fields.booked[0] === '0') {
						booked = false
					} else if(fields.booked[0] === '1') {
						booked = true
					}

					House.findOneAndUpdate({_id: fields.id[0]}, {$set: {name: fields.name[0], area: parseInt(fields.area[0]), description: fields.description[0], latitude: fields.latitude[0], longitude: fields.longitude[0], address: fields.address[0], locality: fields.locality[0], inclusion: fields.inclusion[0], exclusion: fields.exclusion[0], wow: fields.wow[0], property_type: fields.ptype[0], property_rooms: fields.proom[0], room_count: parseInt(fields.roomcount[0]), property_for: fields.pfor[0], poc_name: fields.poc_name[0], poc_contact: fields.poc_contact[0], location_url: fields.short_url[0], amenities: aminityarr, details: detailsarr, prices: prices, booked: booked}}).exec((updateerr, updateresp) => {
						if(updateerr) {
							callback(updateerr)
						} else {
							callback(null)
						}
					})		
				}
			], function (err) {
				if(err) {
					console.log(err)
					res.send({success: 0})
				} else {
					res.send({success: 1})
				}
			})
		}
    })
})

var createHash = function(password, salt){
	return crypto.createHash('sha256', salt).update(password).digest('hex');
}

module.exports = router