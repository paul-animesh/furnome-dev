var express = require('express')
var router = express.Router()
const mongoose = require('mongoose')
const House = mongoose.model('House')
const Locality = mongoose.model('Locality')
const Enquiry = mongoose.model('Enquiry')
const Tenant = mongoose.model("Tenant")
const springedge = require('springedge')
const multiparty = require('multiparty')
const async = require('async')
const moment = require('moment');
const fs = require('fs')
const sgMail = require('@sendgrid/mail')
const { GoogleSpreadsheet } = require('google-spreadsheet');
const doc = new GoogleSpreadsheet('1DBxQzkDhfwC7QrtKctBd2C3lwKhdPcFMTsHv1QOFPhI');
sgMail.setApiKey("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");
doc.useServiceAccountAuth(require('../client_secret.json'));
doc.loadInfo();

/* GET home page. */
router.get('/', function (req, res, next) {
    House.find({}).limit(10).sort({ popularity: -1, date_created: -1 }).exec((err, houses) => {
        res.render('index', {
            meta_title: 'Flat, room for rent, PG Furnished, Houses for rent | No Brokerage | Semi Furnished / Fully Furnished Rental | Furnome',
            meta_desc: 'Hassle Free Accommodations. No owner interference & maintenance issues while you enjoy your living in bangalore',
            houses: houses
        })
    })
})

router.get('/getlocation', function (req, res, next) {
    let query = typeof (req.query.search) != 'undefined' ? req.query.search : "";
    Locality.find({ locality: { $regex: '.*' + query + '.*', $options: "i" } }).distinct('locality', (err, localities) => {
        if (err) {
            res.json({ success: 0 })
        } else {
            res.json({ success: 1, locality: localities })
        }
    })
})

router.get('/listing', function (req, res, next) {
    let gender = req.param('gender')
    let location = req.param('location')
    let house = req.param('house')
    let rooms = req.param('rooms')
    let minbudget = req.param('minbudget')
    let maxbudget = req.param('maxbudget')
    let minroomsize = req.param('minsize')
    let maxroomsize = req.param('maxsize')
    let booked = req.param('booked')
    let amenities = req.param('amenities')
    let sort = req.param('sort')

    let filters = []
    if (typeof (gender) != 'undefined') {
        let glist = gender.split(",")
        if (glist.indexOf("both") >= 0) {
            glist.push("boys")
            glist.push("girls")
        } else {
            glist.push("both")
        }
        filters.push({ "property_for": { $in: glist } })
    }

    if (typeof (location) != 'undefined') {
        filters.push({ "locality": { $regex: '.*' + location + '.*', $options: "i" } })
    }

    if (typeof (house) != 'undefined') {
        let hlist = house.split(",")
        filters.push({ "property_type": { $in: hlist } })
    }

    if (typeof (rooms) != 'undefined') {
        let rlist = rooms.split(",")
        filters.push({ "property_rooms": { $in: rlist } })
    }

    if (typeof (minbudget) != 'undefined' && typeof (maxbudget) != 'undefined') {
        filters.push({ "prices.rent": { $gte: parseInt(minbudget), $lte: parseInt(maxbudget) } })
    } else {
        if (typeof (minbudget) != 'undefined') {
            filters.push({ "prices.rent": { $gte: parseInt(minbudget) } })
        }

        if (typeof (maxbudget) != 'undefined') {
            filters.push({ "prices.rent": { $lte: parseInt(maxbudget) } })
        }
    }

    if (typeof (minroomsize) != 'undefined' && typeof (maxroomsize) != 'undefined') {
        filters.push({ "area": { $gte: parseInt(minroomsize), $lte: parseInt(maxroomsize) } })
    } else {
        if (typeof (minroomsize) != 'undefined') {
            filters.push({ "area": { $gte: parseInt(minroomsize) } })
        }

        if (typeof (maxroomsize) != 'undefined') {
            filters.push({ "area": { $lte: parseInt(maxroomsize) } })
        }
    }

    if (typeof (booked) != 'undefined' && booked === 'false') {
        filters.push({ "booked": false })
    }

    if (typeof (amenities) != 'undefined') {
        let alist = amenities.split(",")
        filters.push({ "amenities": { $in: alist } })
    }

    let sortlist = { popularity: -1 }
    if (typeof (sort) != 'undefined') {
        if (sort == 'a_z') {
            sortlist = { "name": 1 }
        } else if (sort == 'z_a') {
            sortlist = { "name": -1 }
        } else if (sort == 'high_low') {
            sortlist = { "prices.rent": -1 }
        } else if (sort == 'low_high') {
            sortlist = { "prices.rent": 1 }
        }
    }

    filters.push({ "deleted": false })
    filters.push({ "status": true })

    House.find({ $and: filters }).select({ "name": 1, "_id": 1, "property_type": 1, "property_for": 1, "locality": 1, "area": 1, "prices": 1, "details": 1, "date_created": 1, "latitude": 1, "longitude": 1, "images": 1, "property_rooms": 1 }).sort(sortlist).exec((err, houses) => {
        res.render('listing-page', {
            meta_title: 'Property Listing | Furnome.com',
            meta_desc: 'Property Listing | Furnome.com',
            houses: houses
        })
    })
})

router.get('/house/:id', function (req, res, next) {
    let id = req.params.id
    House.findOne({ _id: id }).exec((err, house) => {
        if (err) {
            res.redirect("/")
        }
        let forarr = []
        if (house['property_for'] == 'both') {
            forarr = ["both", "girls", "boys"]
        } else {
            forarr = [house["property_for"]]
        }

        House.find({ $and: [{ locality: house['locality'] }, { property_for: { $in: forarr } }, { property_type: house["property_type"] }, { deleted: false }, { status: true }] }).select({ "name": 1, "_id": 1, "property_type": 1, "property_for": 1, "locality": 1, "area": 1, "prices": 1, "details": 1, "date_created": 1, "latitude": 1, "longitude": 1, "images": 1 }).sort({ date_created: -1 }).limit(10).exec((error, houselist) => {
            if (error) {
                houselist = []
            }
            res.render('description', {
                meta_title: house['name'] + ' House in ' + house['locality'] + ', Bangalore | Furnome.com',
                meta_desc: 'Find ' + house['name'] + ' House in ' + house['locality'] + ', Bangalore at very low price | Furnome.com',
                house: house,
                houselist: houselist
            })
        })
    })
})

router.post('/schedule', function (req, res) {
    let data = req.body
    House.findOneAndUpdate({ _id: data.house_id }, { $inc: { popularity: 1 } }).exec((err, house) => {
        if (err) {
            res.redirect("/")
        }
        let enquiry = new Enquiry()
        enquiry.house_id = house._id
        enquiry.house_name = house.name
        enquiry.name = data.sch_name
        enquiry.email = data.sch_email
        enquiry.phone = data.sch_phone
        let datearr = data.sch_date.split("-")
        let date = datearr[2] + "-" + datearr[1] + "-" + datearr[0]
        enquiry.date = date
        enquiry.time = data.sch_time
        enquiry.poc_name = house.poc_name
        enquiry.poc_contact = house.poc_contact


        getscheduleRows(data, house)

        // let m = moment();
        // m = m.format('MMMM DD YYYY h:mm:ss', true);



        // console.log(sheet.title);
        // console.log(sheet.rowCount);









        enquiry.save((error, result) => {
            // console.log('result',result);
            if (error) {
                res.redirect("/")
            }

            //Send SMS
            let customer = {
                'sender': 'FRNOME',
                'apikey': '36432xa4p75464z7nbnmrsc9a8d70d81qj4',
                'to': [
                    data.sch_phone  //Moblie Numbers 
                ],
                'message': "Property visit scheduled on " + date + " at " + data.sch_time + ". Contact " + house.poc_name + " on " + house.poc_contact + " 30 mins prior. Location: " + house.location_url,
                'format': 'json'
            }

            let house_type = "Full House"
            if (house.property_type == "shared") {
                let house_type = "Shared House"
            } else if (house.property_type == "studio") {
                let house_type = "Studio Appartment"
            }

            let inhouse = {
                'sender': 'FRNOME',
                'apikey': '36432xa4p75464z7nbnmrsc9a8d70d81qj4',
                'to': [
                    house.poc_contact  //Moblie Numbers 

                ],
                'message': data.sch_name + " " + data.sch_phone + " has booked a visit at Location:" + house.location_url + " for " + house_type + " on " + date + " at " + data.sch_time,
                'format': 'json'
            }
            enquiry.mailType = 'schedule'

            sendMail(enquiry);

            springedge.messages.send(customer, 5000, function (errc, responsec) {
                if (errc) {
                    console.log(errc)
                    res.redirect("/house/" + house._id)
                }
                springedge.messages.send(inhouse, 5000, function (erri, responsei) {
                    if (erri) {
                        console.log(erri)
                        res.redirect("/house/" + house._id)
                    }
                    res.redirect("/house/" + house._id)
                });
            });
        })
    })
})

router.get('/about-us', function (req, res, next) {
    res.render('about', {
        meta_title: 'About Us | Furnome.com',
        meta_desc: 'About Us | Furnome.com'
    });
});

router.get('/contact-us', function (req, res, next) {
    res.render('contact', {
        meta_title: 'Contact Us | Furnome.com',
        meta_desc: 'Contact Us | Furnome.com'
    });
});

router.get('/terms-conditions', function (req, res, next) {
    res.render('terms-conditions', {
        meta_title: 'Terms And Conditions | Furnome.com',
        meta_desc: 'Terms And Conditions | Furnome.com'
    });
});

router.get('/privacy-policy', function (req, res, next) {
    res.render('privacy-policy', {
        meta_title: 'Privacy Policy | Furnome.com',
        meta_desc: 'Privacy Policy | Furnome.com'
    });
});

router.get('/faq', function (req, res, next) {
    res.render('faq', {
        meta_title: 'Frequently Asked Questions | Furnome.com',
        meta_desc: 'Frequently Asked Questions | Furnome.com'
    });
});

router.post('/contactus_submit', function (req, res, next) {
    var data = req.body;
    // console.log(data, '================')
    data['mailType'] = 'contact-us';
    getRows(data)
    sendMail(data);
    res.send({ success: 1 })

});

router.get('/tenantinfo', function (req, res, next) {
    res.render('tenant_info', {
        meta_title: 'Frequently Asked Questions | Furnome.com',
        meta_desc: 'Frequently Asked Questions | Furnome.com'
    });
});


async function getRows(data) {
    let m = moment();
    m = m.format('MMMM DD YYYY h:mm:ss', true);
    try {
        const sheet = await doc.sheetsByIndex[1];
        const rows = await sheet.getRows()
        const larryRow = await sheet.addRow({
            'Name': data.name,
            'Email': data.email,
            'Mobile number': data.number,
            'Schedule Date': 'N/A',
            'Building Name': 'N/A',
            'Occupancy': 'N/A',
            'Lead Status': 'N/A',
            'Created Date': m,
            'Budget': 'N/A',
            'Area': 'N/A',
            'Remarks': data.message,
            'Source': 'N/A',
            'Ad link': 'N/A'
        });

        // console.log(sheet.title);
        // console.log(sheet.rowCount);
    }
    catch (e) {
        console.log("error========> ", e)
    }

}








async function getscheduleRows(data, house) {
    // console.log('============')
    // console.log(data)
    // console.log('============')
    let m = moment();
    m = m.format('MMMM DD YYYY h:mm:ss', true);
    try {
        const sheet = await doc.sheetsByIndex[1];
        const rows = await sheet.getRows()
        const larryRow = await sheet.addRow({
            'Name': data.sch_name,
            'Email': data.sch_email,
            'Mobile number': data.sch_phone,
            'Schedule Date': data.sch_date,
            'Building Name': house.name,
            'Occupancy': 'N/A',
            'Lead Status': 'N/A',
            'Created Date': m,
            'Budget': 'N/A',
            'Area': 'N/A',
            'Remarks': 'N/A',
            'Source': 'N/A',
            'Ad link': 'N/A'
        });

        // console.log(sheet.title);
        // console.log(sheet.rowCount);
    }
    catch (e) {
        console.log("error========> ", e)
    }

}









router.post('/informationsave', function (req, res, next) {
    let form = new multiparty.Form()
    form.parse(req, function (err, fields, files) {
        //Request file information
        //{ fieldName: 'uploaded_file',  originalFilename: 'kapil.jpg',  path: '/tmp/x8y45YxeWhtihLJ97wd46_1P.jpg',  headers:    { 'content-disposition': 'form-data; name="uploaded_file"; filename="kapil.jpg"',     'content-type': 'image/jpeg' },  size: 13636 }
        let profile_photo = ""
        let id_proof = ""
        let emp_id_proof = ""
        let timestamp = Date.now()
        async.waterfall([
            function (callback) {
                if (files.profile_photo.length === 1) {
                    let filepath = __dirname + "/../public/tenant/" + timestamp + '_' + files.profile_photo[0].originalFilename
                    fs.rename(files.profile_photo[0].path, filepath, function (err) {
                        if (err) {
                            console.log(err);
                            callback(err)
                        }
                        profile_photo = timestamp + '_' + files.profile_photo[0].originalFilename
                        callback(null)
                    })
                } else {
                    callback(null)
                }
            },
            function (callback) {
                if (files.id_proof.length === 1) {
                    let filepath = __dirname + "/../public/tenant/" + timestamp + '_' + files.id_proof[0].originalFilename
                    fs.rename(files.id_proof[0].path, filepath, function (err) {
                        if (err) {
                            console.log(err);
                            callback(err)
                        }
                        id_proof = timestamp + '_' + files.id_proof[0].originalFilename
                        callback(null)
                    })
                } else {
                    callback(null)
                }
            },
            function (callback) {
                if (files.emp_id_proof.length === 1) {
                    let filepath = __dirname + "/../public/tenant/" + timestamp + '_' + files.emp_id_proof[0].originalFilename
                    fs.rename(files.emp_id_proof[0].path, filepath, function (err) {
                        if (err) {
                            console.log(err);
                            callback(err)
                        }
                        emp_id_proof = timestamp + '_' + files.emp_id_proof[0].originalFilename
                        callback(null)
                    })
                } else {
                    callback(null)
                }
            },
            function (callback) {
                let tenant = new Tenant()
                tenant.name = fields.name
                tenant.gender = fields.gender
                tenant.dob = fields.dob
                tenant.contact = fields.contact
                tenant.whatsapp = fields.whatsapp
                tenant.paytm = fields.paytm
                tenant.email = fields.email
                tenant.profile_photo = profile_photo
                tenant.parent_name = fields.parent_name
                tenant.permanent_address = fields.permanent_address
                tenant.emergency_contact_name = fields.econtact_name
                tenant.emergency_contact_number = fields.econtact_number
                tenant.workplace = fields.workplace
                tenant.blood_group = fields.blood_group
                tenant.id_proof = id_proof
                tenant.emp_id_proof = emp_id_proof
                tenant.save(function (err, result) {
                    if (err) {
                        console.log(err)
                        callback(err)
                    }
                    callback(null)
                })
            }
        ], function (err) {
            if (err) {
                res.redirect("/tenantinfo")
            } else {
                res.redirect("/")
            }
        })
    });
});

router.get('/exitform', function (req, res, next) {
    res.render('exitform')
});

function sendMail(data) {
    if (data.mailType == 'contact-us') {
        var msg = {
            to: 'info@furnome.com',
            from: 'info@furnome.com',
            subject: 'Contact Us | Furnome',
            text: "Name: " + data.name + "<br>Email: " + data.email + "<br>Phone: " + data.number + "<br>Message: " + data.message,
            html: "<html><body><table><tbody><tr><td>Name</td><td>" + data.name + "</td></tr><tr><td>Email</td><td>" + data.email + "</td></tr><tr><td>Phone</td><td>" + data.number + "</td></tr><tr><td>Message</td><td>" + data.message + "</td></tr></tbody></table></body></html>",
        };
        sgMail.send(msg);

        msg = {
            to: data.email,
            from: 'info@furnome.com',
            subject: 'Contact Us | Furnome',
            text: "Dear " + data.name + ", We have received your message. Our Executive will shortly get in touch with you",
            html: "Dear " + data.name + ",<br><br> We have received your message. Our Executive will shortly get in touch with you.<br><br>Regards,<br>Furnome Team",
        };
        sgMail.send(msg);

    }
    else if (data.mailType == 'schedule') {
        var msg = {
            to: 'info@furnome.com',
            from: 'info@furnome.com',
            subject: 'Request for visit schedule',
            text: "Request for visit schedule",
            html: "<html><body><table><tbody><tr><td>House Id</td><td>" + data.house_id + "</td></tr><tr><td>House Name</td><td>" + data.house_name + "</td></tr><tr><td>Customer Name</td><td>" + data.name + "</td></tr><tr><td>Customer Email</td><td>" + data.email + "</td></tr><tr><td>Customer Phone</td><td>" + data.phone + "</td></tr><tr><td>Schedule Date</td><td>" + data.date + "</td></tr><tr><td>Schedule Time</td><td>" + data.time + "</td></tr></tbody></table></body></html>"
        };
        sgMail.send(msg);

    }
}

module.exports = router
