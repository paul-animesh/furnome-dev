const express = require('express')
const path = require('path')
const favicon = require('serve-favicon')
const logger = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const session = require('express-session')
const RedisStore = require('connect-redis')(session)
const mongoose = require('mongoose')
// const csrf = require('csurf')
//connect to mongodb
// mongoose.connect('mongodb://localhost:27017/furnome')
const db = "mongodb+srv://furnome-dev:furnome@007@furnome-dev-xgkpx.mongodb.net/<dbname>?retryWrites=true&w=majority"
const connectDB = async () => {
  try {
      await mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })

      console.log("db conected...")
  }
  catch (err) {
      console.log(err.message)
      process.exit(1)
  }
}
connectDB()
require('./models/models')

const index = require('./routes/index')
const karma = require('./routes/karma')
const auth = require('./routes/auth')
const consolepanel = require('./routes/console')
const owners = require('./routes/owners')

const app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
// app.use(logger('dev'))
app.use(session({
  store: new RedisStore({ host: 'localhost', port: 6379 }),
  secret: 'merak.ai web app',
  //cookie: { maxAge: 60000, expires: new Date(Date.now() + 3600000), httpOnly: true, secure: true}
  cookie: { maxAge: 60000, expires: false, httpOnly: true},
  resave: false,
  saveUninitialized: true
}))
/*app.use(csrf())

app.use(function(req, res, next) {
  res.locals._csrf = req.csrfToken()
  next()
})*/

app.use('/', index)
app.use('/karma', karma)
app.use('/auth', auth)
app.use('/console', consolepanel)
app.use('/owners', owners)


// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   const err = new Error('Not Found')
//   err.status = 404
//   next(err)
// })

// error handlers

// development error handler
// will print stacktrace
// if (app.get('env') === 'development') {
//   app.use(function(err, req, res, next) {
//     /*res.status(err.status || 500)
//     res.render('error', {
//       message: err.message,
//       error: err
//     })*/
//     if (err.status) {
//       const request_url = req.url
//       res.render('404',{url:request_url})
//     } else {
//       res.render('500')
//     }
//   })
// }

// production error handler
// no stacktraces leaked to user
// app.use(function(err, req, res, next) {
//   res.status(err.status || 500)
//   res.render('error', {
//     message: err.message,
//     error: {}
//   })
// })


module.exports = app
