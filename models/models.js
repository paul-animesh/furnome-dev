var mongoose = require('mongoose')

/*
User schema to save user information
*/
const userSchema = new mongoose.Schema({
	username: {type: String, required: true},
	password: {type: String, required: true},
	salt: {type: String, required: true},
	email: {type: String, required: true},
	type: {type: String, required: true, default: 'admin'},
	created_at: {type: Date, default: Date.now}
})


const validateSchema = new mongoose.Schema({
	user_id: String,
	validation: String,
	valid: String,
	created_at: {type: Date, default: Date.now}
})

const ownerSchema = new mongoose.Schema({
	owner_name: String,
	owner_number: String,
	owner_email: String,
	owner_address: String,
	owner_apartment_name: String,
	owner_apartment_floor: String,
	owner_apartment_rooms: String,
	owner_apartment_availability: String,
	owner_apartment_rent: String,
	owner_apartment_deposit: String,
	owner_apartment_date_availability: String,
	owner_apartment_amenities: String
})

const houseSchema = new mongoose.Schema({
	name: {type: String, required: true},
	area: {type: Number, required: true},
	description: {type: String, required: true}, 
	latitude: {type: String, required: true},
	longitude: {type: String, required: true},
	address: {type: String, required: true},
	locality: {type: String, required: true},
	inclusion: {type: String, required: true},
	exclusion: {type: String, required: true},
	wow: {type: String, required: true},
	property_type: {type: String, required: true},
	property_rooms: {type: String, required: true},
	room_count: {type: Number, required: true, default: 0},
	property_for: {type: String, required: true},
	poc_name: {type: String, required: true},
	poc_contact: {type: String, required: true},
	location_url: {type: String, required: true},
	prices: {type: Array, required: true},
	amenities: Array,
	details: Array,
	images: Array,
	booked: {type: Boolean, required: true, default: false},
	date_created: {type: Date, required: true, default: Date.now()},
	status: {type: Boolean, required: true, default: true},
	deleted: {type: Boolean, required: true, default: false},
	popularity: {type: Number, required: true, default: 0}
})

const localitySchema = new mongoose.Schema({
	locality: {type: String, required: true},
	date_created: {type: Date, required: true, default: Date.now()}
})

const enquirySchema = new mongoose.Schema({
	house_id: {type: String, required: true},
	house_name: {type: String, required: true},
	name: {type: String, required: true},
	email: {type: String, required: true},
	phone: {type: String, required: true},
	date: {type: String, required: true},
	time: {type: String, required: true},
	poc_name: {type: String, required: true},
	poc_contact: {type: String, required: true},
	date_created: {type: Date, required: true, default: Date.now()}
})

const tenantSchema = new mongoose.Schema({
	name: {type: String, required: true},
	gender: {type: String, required: true},
	dob: {type: String, required: true},
	contact: {type: String, required: true},
	whatsapp: {type: String, default: ""},
	paytm: {type: String, default: ""},
	email: {type: String, required: true},
	profile_photo: {type: String, default: ""},
	parent_name: {type: String, required: true},
	permanent_address: {type: String, default: ""},
	emergency_contact_name: {type: String, required: true},
	emergency_contact_number: {type: String, required: true},
	workplace: {type: String, default: ""},
	blood_group: {type: String, default: ""},
	id_proof: {type: String, required: true},
	emp_id_proof: {type: String, default: ""},
	date_created: {type: Date, required: true, default: Date.now()}
})

mongoose.model('User', userSchema)
mongoose.model('Owner', ownerSchema)
mongoose.model('Validate', validateSchema)
mongoose.model('House', houseSchema)
mongoose.model('Locality', localitySchema)
mongoose.model('Enquiry', enquirySchema)
mongoose.model("Tenant", tenantSchema)